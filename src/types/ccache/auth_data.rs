use super::address::*;

/// Container that encapsules different types of preauthentication data structures.
pub type AuthData = Address;
