use super::super::kerberos_flags::*;

/// (*TicketFlags*) Flags for tickets.
pub type TicketFlags = KerberosFlags;
pub(crate) type TicketFlagsAsn1 = KerberosFlagsAsn1;
