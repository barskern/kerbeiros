use super::primitives::kerberos_string::*;

/// (*Realm*) Kerberos realm.
pub type Realm = KerberosString;
pub(crate) type RealmAsn1 = KerberosStringAsn1;
