//! Options used by the message `AsReq` (not implemented yet).

pub const RESERVED: u32 = 0x80;
pub const USE_SESSION_KEY: u32 = 0x40;
pub const MUTUAL_REQUIRED: u32 = 0x20;